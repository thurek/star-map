# About the app
A hobby project linking the development of a web application with a 3D visualization of the Suns nearest star cluster.

* The server is a **Spring Boot** app meant to be deployed through Tomcat.
    * The persistence layer uses Hibernate with an in-memory database for presentation purpose.
	* Stars and constellation data is provided in CVS files, which are parsed and injected into the DB on startup.

* The client is a **Angular** app.
    * Uses bootstrap for its grid layout capability.
	* Three.js used to visualize the star coordinates on a 3D canvas.
	
Data used from following websites:

* [heasarc.gsfc.nasa.gov](https://heasarc.gsfc.nasa.gov/db-perl/W3Browse/w3table.pl?tablehead=name%3Dcns3&Action=More+Options)
* [gea.esac.esa.int](https://gea.esac.esa.int/archive/)

# How to launch

## Start the server
* go to ```./core```
* use following command ```./gradlew build```
* afterwards launch the server locally using ```./gradlew run```

## Start the client
* install node package manager
* install Angular CLI: ```npm i -g @angular/cli```
* go to ```./ui-client```
* run command ```ng serve```

## Access app
* open a new browser window and visit: `localhost:4200`