package com.starmap.core.data;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.starmap.core.constellation.Constellation;
import com.starmap.core.constellation.ConstellationRepository;
import com.starmap.core.star.Star;
import com.starmap.core.star.StarRepository;

@Component
public class DataLoader {

	Logger logger = LoggerFactory.getLogger(DataLoader.class);

	private StarRepository starRepository;
	private ConstellationRepository constellationRepository;

	@Autowired
	public DataLoader(StarRepository starRepository, ConstellationRepository constellationRepository)
			throws IOException {
		this.starRepository = starRepository;
		this.constellationRepository = constellationRepository;

		loadStars();
		loadConstellations();
		linkStarsToConstellations();
	}

	private void loadStars() throws IOException {
		List<Star> stars = loadEntityFromCSV(Star.class, "star_data.csv");
		starRepository.saveAll(stars);
	}

	private void loadConstellations() throws IOException {
		List<Constellation> constellations = loadEntityFromCSV(Constellation.class, "constellations.csv");
		constellationRepository.saveAll(constellations);
	}

	private void linkStarsToConstellations() throws IOException {
		List<Map<String, String>> hasConstellations = loadJsonFromCsv("has_constellation.csv");
		List<Star> stars = new ArrayList<>(hasConstellations.size());
		for (Map<String, String> hasConstellation : hasConstellations) {
			Star star = linkStarToConstellation(hasConstellation.get("gl"), hasConstellation.get("con"));
			if (star != null)
				stars.add(star);
		}
		starRepository.saveAll(stars);
	}

	private Star linkStarToConstellation(String glieseNumber, String constellationAbbrevation) {
		if (glieseNumber == null || glieseNumber.isBlank() || constellationAbbrevation == null
				|| constellationAbbrevation.isBlank())
			return null;

		Star star = starRepository.findByName(glieseNumber);

		if (star == null)
			return null;

		Constellation constellation = constellationRepository.findByAbbreviation(constellationAbbrevation);
		if (constellation == null)
			return null;

		star.setConstellation(constellation);
		return star;
	}
	
	private static <T> List<T> loadEntityFromCSV(Class<T> typeParameterClass, String filename) throws IOException {
		File input = ResourceUtils.getFile("classpath:" + filename);
		CsvMapper mapper = new CsvMapper();
		mapper.enable(CsvParser.Feature.IGNORE_TRAILING_UNMAPPABLE);
		CsvSchema schema = mapper.schemaFor(typeParameterClass).withHeader().withColumnReordering(true);
		ObjectReader reader = mapper.readerFor(typeParameterClass).with(schema);
		return reader.<T>readValues(input).readAll();
	}

	private static List<Map<String, String>> loadJsonFromCsv(String filename) throws IOException {
		File input = ResourceUtils.getFile("classpath:" + filename);
		CsvSchema csv = CsvSchema.emptySchema().withHeader();
		CsvMapper csvMapper = new CsvMapper();
		MappingIterator<Map<String, String>> mappingIterator = csvMapper.reader().forType(Map.class).with(csv)
				.readValues(input);
		return mappingIterator.readAll();
	}

}
