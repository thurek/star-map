package com.starmap.core.star;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.starmap.core.constellation.Constellation;

@Entity
public class Star {

	public static final double R_SUN = 2.25461 * Math.pow(10, -8);

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	/**
	 * Catalog Designation
	 */
	private String name;

	/**
	 * Apparent Magnitude
	 */
	private Float appMag;

	/**
	 * Declination
	 */
	private String dec;

	/**
	 * Right Ascension
	 */
	private String ra;

	/**
	 * Resulting Parallax (mas)
	 */
	private Float resultParallax;

	/**
	 * Standard Error of Resulting Parallax
	 */
	private Float resultParallaxError;

	/**
	 * The spectral type (e.g., G2 V) or Luyten color class (e.g., m)
	 */
	private String spectType;

	/**
	 * Galactic Longitude
	 */
	private Float lii;

	/**
	 * Galactic Latitude
	 */
	private Float bii;

	/**
	 * The component designation (A, B, C ...) if in a visual binary system
	 */
	private String component;

	/**
	 * The reliability of the distance.
	 */
	private String distanceCode;

	/**
	 * The total proper motion in seconds of arc per year.
	 */
	private Float totProperMotion;

	/**
	 * Proper Motion Uncertainty Flag [:]
	 */
	private String flagProperMotion;

	/**
	 * The direction angle of proper motion, in degrees, using the standard
	 * convention (N is 0, increasing eastwards).
	 */
	private Float dirProperMotion;

	/**
	 * The heliocentric radial velocity in km/s.
	 */
	private Float radialVelocity;

	/**
	 * Information regarding the radial velocity and possible close binary status is
	 * coded.
	 */
	private String radialVelocityCode;

	/**
	 * The reference code for the origin of the spectral type.
	 */
	private String refSpectType;

	/**
	 * If blank, this means that the apparent magnitude is in the Johnson V band.
	 * Otherwise, one of the following codes for apparent magnitude is used.
	 */
	private String appMagCode;

	/**
	 * If `J', this means that the quoted magnitude is a joint magnitude.
	 */
	private String appMagJoint;

	/**
	 * The B-V color.
	 */
	private Float bvColor;

	/**
	 * The reference code for the origin of the B-V color.
	 */
	private String bvColorCode;

	/**
	 * If `J', this means that the quoted color is a joint color.
	 */
	private String bvColorJoint;

	/**
	 * The U-B color
	 */
	private Float ubColor;

	/**
	 * he reference code for the U-B color.
	 */
	private String ubColorCode;

	/**
	 * If `J', this means that the quoted color is a joint color
	 */
	private String ubColorJoint;

	/**
	 * The R-I color
	 */
	private Float riColor;

	/**
	 * The reference code for the R-I color
	 */
	private String riColorCode;

	/**
	 * If `J', this means that the quoted color is a joint color.
	 */
	private String riColorJoint;

	/**
	 * The trigonometric parallax, TP, in milliseconds of arc. To calculate the
	 * distance D in parsecs based on this value, D = 1000/TP.
	 */
	private Float trigParallax;

	/**
	 * The standard error of the trigonometric parallax, in milliseconds of arc.
	 */
	private Float trigParallaxError;

	/**
	 * The reference code for the resulting parallax.
	 */
	private String resultParallaxCode;

	/**
	 * The absolute visual magnitude, M(V) based on the apparent magnitude V and the
	 * resulting parallax, RP. The relation is M(V) = V + Log10(RP) -10.0, where RP
	 * is in milliarcsecs.
	 */
	private Float absVmag;

	/**
	 * The quality of the absolute (visual) magnitude, coded as follows (where s.e.
	 * is the standard error).
	 */
	private String absVmagCode;

	/**
	 * The U space velocity component in the Galactic plane and directed to the
	 * Galactic center.
	 */
	private Integer uvel;

	/**
	 * The V space velocity component in the Galactic plane and in the direction of
	 * Galactic rotation.
	 */
	private Integer vvel;

	/**
	 * The W space velocity component perpendicular to the Galactic plane and in the
	 * direction of the North Galactic Pole.
	 */
	private Integer wvel;

	/**
	 * The Henry Draper catalog entry number.
	 */
	private Integer hdNumber;

	/**
	 * The Durchmusterung zone and number.
	 */
	private String dmNumber;

	/**
	 * The Giclas catalog entry number.
	 */
	private String giclasNumber;

	/**
	 * The LHS catalog entry number.
	 */
	private String lhsNumber;

	/**
	 * Alternative catalog designations.
	 * 
	 */
	private String otherName;

	/**
	 * Additional identifications (LTT, LFT, Wolf, Ross, etc.) and other remarks.
	 */
	private String remarks;

	/**
	 * The Browse object classification.
	 */
	private String clazz;

	@ManyToOne
	@JoinColumn(name = "CONSTELLATION_ID", referencedColumnName = "ID")
	@JsonBackReference
	private Constellation constellation;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getAppMag() {
		return appMag;
	}

	public void setAppMag(Float appMag) {
		this.appMag = appMag;
	}

	public String getDec() {
		return dec;
	}

	public void setDec(String dec) {
		this.dec = dec;
	}

	public String getRa() {
		return ra;
	}

	public void setRa(String ra) {
		this.ra = ra;
	}

	public Float getResultParallax() {
		return resultParallax;
	}

	public void setResultParallax(Float resultParallax) {
		this.resultParallax = resultParallax;
	}

	public Float getResultParallaxError() {
		return resultParallaxError;
	}

	public void setResultParallaxError(Float resultParallaxError) {
		this.resultParallaxError = resultParallaxError;
	}

	public String getSpectType() {
		return spectType;
	}

	public void setSpectType(String spectType) {
		this.spectType = spectType;
	}

	public Float getLii() {
		return lii;
	}

	public void setLii(Float lii) {
		this.lii = lii;
	}

	public Float getBii() {
		return bii;
	}

	public void setBii(Float bii) {
		this.bii = bii;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getDistanceCode() {
		return distanceCode;
	}

	public void setDistanceCode(String distanceCode) {
		this.distanceCode = distanceCode;
	}

	public Float getTotProperMotion() {
		return totProperMotion;
	}

	public void setTotProperMotion(Float totProperMotion) {
		this.totProperMotion = totProperMotion;
	}

	public String getFlagProperMotion() {
		return flagProperMotion;
	}

	public void setFlagProperMotion(String flagProperMotion) {
		this.flagProperMotion = flagProperMotion;
	}

	public Float getDirProperMotion() {
		return dirProperMotion;
	}

	public void setDirProperMotion(Float dirProperMotion) {
		this.dirProperMotion = dirProperMotion;
	}

	public Float getRadialVelocity() {
		return radialVelocity;
	}

	public void setRadialVelocity(Float radialVelocity) {
		this.radialVelocity = radialVelocity;
	}

	public String getRadialVelocityCode() {
		return radialVelocityCode;
	}

	public void setRadialVelocityCode(String radialVelocityCode) {
		this.radialVelocityCode = radialVelocityCode;
	}

	public String getRefSpectType() {
		return refSpectType;
	}

	public void setRefSpectType(String refSpectType) {
		this.refSpectType = refSpectType;
	}

	public String getAppMagCode() {
		return appMagCode;
	}

	public void setAppMagCode(String appMagCode) {
		this.appMagCode = appMagCode;
	}

	public String getAppMagJoint() {
		return appMagJoint;
	}

	public void setAppMagJoint(String appMagJoint) {
		this.appMagJoint = appMagJoint;
	}

	public Float getBvColor() {
		return bvColor;
	}

	public void setBvColor(Float bvColor) {
		this.bvColor = bvColor;
	}

	public String getBvColorCode() {
		return bvColorCode;
	}

	public void setBvColorCode(String bvColorCode) {
		this.bvColorCode = bvColorCode;
	}

	public String getBvColorJoint() {
		return bvColorJoint;
	}

	public void setBvColorJoint(String bvColorJoint) {
		this.bvColorJoint = bvColorJoint;
	}

	public Float getUbColor() {
		return ubColor;
	}

	public void setUbColor(Float ubColor) {
		this.ubColor = ubColor;
	}

	public String getUbColorCode() {
		return ubColorCode;
	}

	public void setUbColorCode(String ubColorCode) {
		this.ubColorCode = ubColorCode;
	}

	public String getUbColorJoint() {
		return ubColorJoint;
	}

	public void setUbColorJoint(String ubColorJoint) {
		this.ubColorJoint = ubColorJoint;
	}

	public Float getRiColor() {
		return riColor;
	}

	public void setRiColor(Float riColor) {
		this.riColor = riColor;
	}

	public String getRiColorCode() {
		return riColorCode;
	}

	public void setRiColorCode(String riColorCode) {
		this.riColorCode = riColorCode;
	}

	public String getRiColorJoint() {
		return riColorJoint;
	}

	public void setRiColorJoint(String riColorJoint) {
		this.riColorJoint = riColorJoint;
	}

	public Float getTrigParallax() {
		return trigParallax;
	}

	public void setTrigParallax(Float trigParallax) {
		this.trigParallax = trigParallax;
	}

	public Float getTrigParallaxError() {
		return trigParallaxError;
	}

	public void setTrigParallaxError(Float trigParallaxError) {
		this.trigParallaxError = trigParallaxError;
	}

	public String getResultParallaxCode() {
		return resultParallaxCode;
	}

	public void setResultParallaxCode(String resultParallaxCode) {
		this.resultParallaxCode = resultParallaxCode;
	}

	public Float getAbsVmag() {
		return absVmag;
	}

	public void setAbsVmag(Float absVmag) {
		this.absVmag = absVmag;
	}

	public String getAbsVmagCode() {
		return absVmagCode;
	}

	public void setAbsVmagCode(String absVmagCode) {
		this.absVmagCode = absVmagCode;
	}

	public Integer getUvel() {
		return uvel;
	}

	public void setUvel(Integer uvel) {
		this.uvel = uvel;
	}

	public Integer getVvel() {
		return vvel;
	}

	public void setVvel(Integer vvel) {
		this.vvel = vvel;
	}

	public Integer getWvel() {
		return wvel;
	}

	public void setWvel(Integer wvel) {
		this.wvel = wvel;
	}

	public Integer getHdNumber() {
		return hdNumber;
	}

	public void setHdNumber(Integer hdNumber) {
		this.hdNumber = hdNumber;
	}

	public String getDmNumber() {
		return dmNumber;
	}

	public void setDmNumber(String dmNumber) {
		this.dmNumber = dmNumber;
	}

	public String getGiclasNumber() {
		return giclasNumber;
	}

	public void setGiclasNumber(String giclasNumber) {
		this.giclasNumber = giclasNumber;
	}

	public String getLhsNumber() {
		return lhsNumber;
	}

	public void setLhsNumber(String lhsNumber) {
		this.lhsNumber = lhsNumber;
	}

	public String getOtherName() {
		return otherName;
	}

	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	
	public Constellation getConstellation() {
		return constellation;
	}

	public void setConstellation(Constellation constellation) {
		this.constellation = constellation;
	}

	public double[] getRgbColor() {
		if (this.bvColor == null)
			return new double[] { 0.5, 0.5, 0.5 };

		double t = 4600 * ((1 / ((0.92 * bvColor) + 1.7)) + (1 / ((0.92 * bvColor) + 0.62)));
		double x = 0;
		double y = 0;

		if (t >= 1667 && t <= 4000) {
			x = ((-0.2661239 * Math.pow(10, 9)) / Math.pow(t, 3)) + ((-0.2343580 * Math.pow(10, 6)) / Math.pow(t, 2))
					+ ((0.8776956 * Math.pow(10, 3)) / t) + 0.179910;
		} else if (t > 4000 && t <= 25000) {
			x = ((-3.0258469 * Math.pow(10, 9)) / Math.pow(t, 3)) + ((2.1070379 * Math.pow(10, 6)) / Math.pow(t, 2))
					+ ((0.2226347 * Math.pow(10, 3)) / t) + 0.240390;
		}

		if (t >= 1667 && t <= 2222) {
			y = -1.1063814 * Math.pow(x, 3) - 1.34811020 * Math.pow(x, 2) + 2.18555832 * x - 0.20219683;
		} else if (t > 2222 && t <= 4000) {
			y = -0.9549476 * Math.pow(x, 3) - 1.37418593 * Math.pow(x, 2) + 2.09137015 * x - 0.16748867;
		} else if (t > 4000 && t <= 25000) {
			y = 3.0817580 * Math.pow(x, 3) - 5.87338670 * Math.pow(x, 2) + 3.75112997 * x - 0.37001483;
		}

		double Y = 1.0;
		double X = (y == 0) ? 0 : (x * Y) / y;
		double Z = (y == 0) ? 0 : ((1 - x - y) * Y) / y;

		double r = 3.2406 * X - 1.5372 * Y - 0.4986 * Z;
		double g = -0.9689 * X + 1.8758 * Y + 0.0415 * Z;
		double b = 0.0557 * X - 0.2040 * Y + 1.0570 * Z;

		double R = (r <= 0.0031308) ? 12.92 * r : 1.055 * Math.pow(r, 1 / 0.5) - 0.055;
		double G = (g <= 0.0031308) ? 12.92 * g : 1.055 * Math.pow(g, 1 / 0.5) - 0.055;
		double B = (b <= 0.0031308) ? 12.92 * b : 1.055 * Math.pow(b, 1 / 0.5) - 0.055;

		return new double[] { R, G, B };
	}

	public double getX() {
		if (bii == null || lii == null)
			return 0;
		return R_SUN - getD() * Math.cos(bii.doubleValue()) * Math.cos(lii.doubleValue());
	}

	public double getY() {
		if (bii == null || lii == null)
			return 0;
		return getD() * Math.cos(bii.doubleValue()) * Math.sin(lii.doubleValue());
	}

	public double getZ() {
		if (bii == null)
			return 0;
		return getD() * Math.sin(bii.doubleValue());
	}

	private double getD() {
		if (resultParallax == null)
			return 0;
		return 1 / resultParallax.doubleValue();
	}

}
