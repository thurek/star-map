package com.starmap.core.star;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api")
@CrossOrigin(origins = "http://localhost:4200")
public class StarController {

	@Autowired
	private StarRepository starRepository;

	@GetMapping("/stars")
	public List<Star> getStars() {
		return (List<Star>) starRepository.findAll();
	}

}
