package com.starmap.core.star;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StarRepository extends CrudRepository<Star, Long> {
	

	Star findByName(String name);
	
}
