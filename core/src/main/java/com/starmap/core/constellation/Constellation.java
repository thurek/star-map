package com.starmap.core.constellation;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.starmap.core.star.Star;

@Entity
public class Constellation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	/**
	 * Latin name
	 */
	private String latinName;
	
	/**
	 * Abbreviation
	 */
	private String abbreviation;
	
	@OneToMany(mappedBy = "constellation")
	@JsonManagedReference
	private List<Star> stars;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public String getLatinName() {
		return latinName;
	}
	
	public void setLatinName(String latinName) {
		this.latinName = latinName;
	}
	
	
	public String getAbbreviation() {
		return abbreviation;
	}
	
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public List<Star> getStars() {
		return stars;
	}

	public void setStars(List<Star> stars) {
		this.stars = stars;
	}
	
	

}
