package com.starmap.core.constellation;

import org.springframework.beans.factory.annotation.Value;


public interface ConstellationSummary {

	public String getLatinName();
	public String getAbbreviation();
	
	@Value("#{target.stars.![id]}")
	public int[] getStarIds();
	
}
