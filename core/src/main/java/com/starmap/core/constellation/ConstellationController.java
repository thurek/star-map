package com.starmap.core.constellation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "api")
@CrossOrigin(origins = "http://localhost:4200")
public class ConstellationController {

	@Autowired
	private ConstellationRepository constellationRepository;

	@GetMapping("/constellations")
	public List<Constellation> getConstellations() {
		return (List<Constellation>) constellationRepository.findAll();
	}
	
	@GetMapping("/constellationSummaries")
	public List<ConstellationSummary> getConstellationSummaries() {
		return (List<ConstellationSummary>) constellationRepository.findAllBy();
	}
	
}
