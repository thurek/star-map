package com.starmap.core.constellation;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConstellationRepository extends CrudRepository<Constellation, Long> {

	Constellation findByAbbreviation(String abbreviation);
	
	Iterable<ConstellationSummary> findAllBy();
	
}
