import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConstellationSummary } from './constellation-summary';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConstellationService {

  constructor(private http: HttpClient) { }

  public findAllSummaries(): Observable<ConstellationSummary[]> {
    return this.http.get<ConstellationSummary[]>('api/constellationSummaries');
  }

}
