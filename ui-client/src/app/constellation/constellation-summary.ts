export class ConstellationSummary {

    abbreviation: string;
    latinName: string;
    starIds: number[];

}
