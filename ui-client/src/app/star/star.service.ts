import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Star } from './star';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StarService {

  constructor(private http: HttpClient) { }

  public findAll(): Observable<Star[]> {
    return this.http.get<Star[]>('api/stars');
  }

}
