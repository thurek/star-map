export class Star {

    id: number;

    name: string;

    x: number;

    y: number;

    z: number;

    rgbColor: number[];

    appMag: number;

    dec: string;

    ra: string;

    resultParallax: number;

    resultParallaxError: number;

    spectType: string;

    lii: number;

    bii: number;

    component: string;

    distanceCode: string;

    totProperMotion: number;

    flagProperMotion: string;

    dirProperMotion: number;

    radialBelocity: number;

    radialVelocityCode: string;

    refSpectType: string;

    appMagCode: string;

    appMagJoint: string;

    bvColor: number;

    bvColorCode: string;

    bvColorJoint: string;

    ubColor: number;

    ubColorCode: string;

    ubColorJoint: string;

    riColor: number;

    riColorCode: string;

    riColorJoint: string;

    trigParallax: number;

    trigParallaxError: number;

    resultParallaxCode: string;

    absVmag: number;

    absVmagCode: string;

    uvel: number;

    vvel: number;

    wvel: number;

    hdNumber: number;

    dmNumber: string;

    giclasNumber: string;

    lhsNumber: string;

    otherName: string;

    remarks: string;

    clazz: string;

}
