import { Component, ElementRef, AfterViewInit, ViewChild, HostListener } from '@angular/core';
import { Star } from '../star/star';
import { StarService } from '../star/star.service';
import { ConstellationSummary } from '../constellation/constellation-summary';
import { ConstellationService } from '../constellation/constellation.service';
import * as THREE from 'three';
import { CSS2DObject, CSS2DRenderer } from 'three/examples/jsm/renderers/CSS2DRenderer.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

@Component({
  selector: 'app-star-map',
  templateUrl: './star-map.component.html',
  styleUrls: ['./star-map.component.sass']
})
export class StarMapComponent implements AfterViewInit {
  @ViewChild('canvas') canvasRef: ElementRef;
  stars: Star[];
  constellations: ConstellationSummary[];

  renderer = null;
  labelRenderer = null;
  scene = null;
  camera = null;
  controls = null;
  raycaster = new THREE.Raycaster();
  mouse = new THREE.Vector2();
  points = null;
  colors = [];
  selectedObject = null;
  selectedObjectLabel = null;
  selectedConstellationSummary = null;
  selectedConstellationLine = null;
  selectedConstellationLabels = [];
  starMap = {};

  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }

  constructor(private starService: StarService, private constellationService: ConstellationService) {
    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
  }

  ngAfterViewInit(): void {
    this.starService.findAll().subscribe(data => {
      this.stars = data;
      this.configRaycaster();
      this.configScene();
      this.configCamera();
      this.configRenderer();
      this.configLabelRenderer();
      this.configControls();
      this.createDots();
      this.animate();
    });
    this.loadConstellationSummaries();
  }

  loadConstellationSummaries(): void {
    this.constellationService.findAllSummaries().subscribe(data => {
      this.constellations = data;
    })
  }

  configScene(): void {
    this.scene.background = new THREE.Color(0x000);
  }

  configRaycaster(): void {
    this.raycaster.params.Points.threshold = 0.2;
  }

  configCamera(): void {
    this.camera.aspect = this.calculateAspectRatio();
    this.camera.updateProjectionMatrix();
    this.camera.position.set(-15, 10, 15);
    this.camera.lookAt(this.scene.position);
  }

  configRenderer(): void {
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas,
      antialias: true,
      alpha: true
    });
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setClearColor(0x000000, 0);
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
  }

  configLabelRenderer(): void {
    this.labelRenderer = new CSS2DRenderer();
    this.labelRenderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    this.labelRenderer.domElement.style.position = 'absolute';
    this.labelRenderer.domElement.style.top = '0px';
    this.labelRenderer.domElement.style.pointerEvents = 'none';
    this.canvas.parentElement.appendChild(this.labelRenderer.domElement);
  }

  private calculateAspectRatio(): number {
    const height = this.canvas.clientHeight;
    if (height === 0) {
      return 0;
    }
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }

  configControls(): void {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.autoRotate = false;
    this.controls.enableZoom = true;
    this.controls.enablePan = true;
    this.controls.update();
  }

  animate(): void {
    window.requestAnimationFrame(() => this.animate());
    this.controls.update();
    this.renderer.render(this.scene, this.camera);
    this.labelRenderer.render(this.scene, this.camera);
  }


  createDots(): void {
    const dotGeometry = new THREE.Geometry();

    this.stars.forEach((star, index) => {
      this.starMap[star.id] = index;

      dotGeometry.vertices.push(new THREE.Vector3(star.x * 1000, star.y * 1000, star.z * 1000));

      const color = new THREE.Color(star.rgbColor[0], star.rgbColor[1], star.rgbColor[2]);
      this.colors.push(color);
      dotGeometry.colors.push(color);
    });

    const dotMaterial = new THREE.PointsMaterial({ size: 2, sizeAttenuation: false, vertexColors: true });
    this.points = new THREE.Points(dotGeometry, dotMaterial);
    this.scene.add(this.points);
  }


  getIntersects(x: number, y: number): THREE.Intersection[] {

    x = (x / this.canvas.clientWidth) * 2 - 1;
    y = - (y / this.canvas.clientHeight) * 2 + 1;

    this.mouse.x = x;
    this.mouse.y = y;

    this.raycaster.setFromCamera(this.mouse, this.camera);

    return this.raycaster.intersectObject(this.points, true);
  }

  @HostListener('mousemove', ['$event'])
  onMouseMove(event): void {
    if (this.selectedObject) {
      this.points.geometry.colors[this.selectedObject.index] = this.colors[this.selectedObject.index];
      this.selectedObject = null;
      this.points.geometry.colorsNeedUpdate = true;
    }

    const intersects = this.getIntersects(event.clientX, event.clientY);
    if (intersects.length > 0) {
      this.selectedObject = intersects[0];
      this.points.geometry.colors[this.selectedObject.index] = new THREE.Color(0xFF0000);
      this.points.geometry.colorsNeedUpdate = true;
    }
  }

  @HostListener('click', ['$event'])
  onClick(event): void {
    if (this.selectedObject) {
      const star = this.stars[this.selectedObject.index];
      if (this.selectedObjectLabel) {// remove old label if exists
        this.scene.remove(this.selectedObjectLabel);
      }

      this.selectedObjectLabel = this.addLabelToStar(star, this.selectedObject.point);
      this.controls.target.set(this.selectedObject.point.x, this.selectedObject.point.y, this.selectedObject.point.z);
    }
  }

  addLabelToStar(star: Star, point: any): CSS2DObject {
    const element = document.createElement('span');
    element.className = 'star-label';
    element.textContent = star.name
    element.style.color = 'rgb(' + star.rgbColor[0] * 255 + ',' + star.rgbColor[1] * 255 + ',' + star.rgbColor[2] * 255 + ')';

    const label = new CSS2DObject(element);
    label.position.copy(point);
    this.scene.add(label);
    return label;
  }

  resetPosition(): void {
    this.controls.target.set(0, 0, 0);
  }

  showConstellation(constellationSummary: ConstellationSummary): void {
    this.selectedConstellationSummary = constellationSummary;
    const constellationPoints = [];

    this.clearConstellationFromScene();

    this.selectedConstellationSummary.starIds.forEach(starId => {
      const point = this.points.geometry.vertices[starId - 1];
      constellationPoints.push(point);
      this.selectedConstellationLabels.push(this.addLabelToStar(this.stars[starId - 1], point));
    });

    const geometry = new THREE.BufferGeometry().setFromPoints(constellationPoints);
    this.selectedConstellationLine = new THREE.Line(geometry, new THREE.LineBasicMaterial({ color: 0xAAAAAA }));
    this.scene.add(this.selectedConstellationLine);
  }

   clearConstellations(): void {
    this.selectedConstellationSummary = null;
    this.clearConstellationFromScene();
  }

  clearConstellationFromScene(): void {
    if (this.selectedConstellationLine) {// remove old constellation from scene if exists
      this.scene.remove(this.selectedConstellationLine);
    }

    if (this.selectedConstellationLabels.length > 0) {// remove old constellation labels
      this.selectedConstellationLabels.forEach(label => {
        this.scene.remove(label);
      });
      this.selectedConstellationLabels = [];
    }
  }

}
